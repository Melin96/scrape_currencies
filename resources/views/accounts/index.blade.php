@extends('layouts.app')

@section('content')

    <div class="content">
        <div class="container">
            <div class="d-flex justify-content-end">
                <form class="form-horizontal" role="form" method="get" action="/">
                    <input type="date" @if (request()->from) value="{{ request()->from }}" @endif name="from" placeholder="Date From" class="fromDate datepicker" />
                    <input type="date" @if (request()->to) value="{{ request()->to }}" @endif name="to" placeholder="Date To" class="toDate datepicker ml-lg-4" />
                    <button type="submit" class="ml-lg-4 btn btn-secondary">Submit</button>
                </form>
            </div>
            @if($currencies->count() > 0)
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">valuteID</th>
                        <th scope="col">numCode</th>
                        <th scope="col">charCode</th>
                        <th scope="col">name</th>
                        <th scope="col">value</th>
                        <th scope="col">date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($currencies as $currency)
                        <tr>
                            <td>{{$currency->valuteID}}</td>
                            <td>{{$currency->numCode}}</td>
                            <td>{{$currency->charCode}}</td>
                            <td>{{$currency->name}}</td>
                            <td>{{$currency->value}}</td>
                            <td>{{date("d-m-Y", $currency->date)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$currencies->links()}}
            @else
                <h3>Empty data!</h3>
            @endif
        </div>
    </div>

@endsection
