## Set up project

- Run command in terminal (composer install)
- Create .env file from .env.example
- Run command (php artisan key:generate)
- In the .env file add database name
- Create database
- Run command (php artisan migrate)
- Run command (php artisan passport:install)
- Run command to insert currencies in database for last 30 days (php artisan get:currencies)
- Run command or create virtual host to run project (php artisan serve)
- Now you can register and login

## Rest Api

- oauth/token run and fill data
  (grant_type, client_id, client_secret, username, password)
  to take access_token
- As user authorised, you can run the commands below
- currencies/ValuteID/from/to (example: api/currencies/R01010/10.09.2021/20.09.2021)
- api/currencies (make new currency - POST)
- api/currencies/{id} (update - PUT)
- api/currencies/{id} (destroy - DELETE)
