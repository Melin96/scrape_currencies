<?php


namespace App\Classes\Commands;



use App\Services\Currencies;
use App\Services\ScrapeInterface;
use Illuminate\Contracts\Container\BindingResolutionException;

class ScrapeCurrencies
{
    /**
     * @throws BindingResolutionException
     */
    public function run()
    {
        app()->bind(ScrapeInterface::class, Currencies::class);

        $scrapeInterface = app()->make(ScrapeInterface::class);

        $scrapeInterface->scrape();
    }
}
