<?php

namespace App\Http\Controllers;

use App\Http\Requests\CurrencyRequest;
use App\Models\Currency;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Carbon\Carbon;

class AccountsController extends Controller
{

    /**
     * @param string $valuteID
     * @param string $from
     * @param string $to
     * @return JsonResponse
     */
    public function find(string $valuteID, string $from, string $to): JsonResponse
    {
        $currencies = Currency::where('valuteID', $valuteID)
            ->where('date', '<=', Carbon::create($to)->timestamp)
            ->where('date', '>=', Carbon::create($from)->timestamp)
            ->selectRaw("valuteID, numCode, charCode, name, value, currencies.valuteID, DATE_FORMAT(from_unixtime(date), '%d-%m-%Y') as dateTime")
            ->orderBy('date')
            ->cursor();

        $response = [
            "data" => $currencies
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * @param CurrencyRequest $request
     * @return JsonResponse
     */
    public function store(CurrencyRequest $request): JsonResponse
    {
        Currency::create($request->all());
        $response = [
            "response" => "Data Created"
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * @param CurrencyRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(CurrencyRequest $request, int $id): JsonResponse
    {
        $currency = Currency::find($id);
        if(!$currency) {
            return response()->json(["response" => "Data Not Found"], Response::HTTP_OK);
        }
        $currency->update($request->all());

        return response()->json(["response" => "Data Updated"], Response::HTTP_OK);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $currency = Currency::find($id);
        if(!$currency) {
            return response()->json(["response" => "Data Not Found"], Response::HTTP_OK);
        }
        $currency->delete();

        return response()->json(["response" => "Deleted"], Response::HTTP_OK);
    }
}
