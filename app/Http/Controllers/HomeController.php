<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Scopes\CurrenciesScope;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;

class HomeController extends Controller
{
    const PAGINATE_COUNT = 15;

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $currencies = Currency::query()
            ->withGlobalScope(CurrenciesScope::class, new CurrenciesScope($request))
            ->paginate(self::PAGINATE_COUNT)
            ->appends(request()->except('page'));

        return view('accounts.index', compact('currencies'));
    }
}
