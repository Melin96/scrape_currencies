<?php


namespace App\Services;


use App\Models\Currency;
use Carbon\Carbon;
use Goutte\Client;

class Currencies implements ScrapeInterface
{
    private $url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=';
    private $num = '30';

    public function scrape() {

        for ($index = $this->num ; $index >= 0; $index--) {

            $unix = Carbon::now()->subDays($index)->timestamp;
            $date = date("d-m-Y",$unix);
            $unixDb = Carbon::create($date)->timestamp;
            $currencies = Currency::where('date', $unixDb)->cursor();

            if(!$currencies->count() > 0) {

                $client = new Client();
                $crawler = $client->request('GET', $this->url.$date);

                $crawler->filter('ValCurs')->each(function ($node) {
                    $dateTime = $node->attr('Date');
                    $currencies = Currency::where('date', Carbon::create($dateTime)->timestamp);

                    if(!$currencies->count() > 0) {

                        $node->filter('Valute')->each(function ($i) use ($dateTime) {
                            Currency::create([
                                'valuteID' => $i->attr('ID'),
                                'numCode' => $i->filter('NumCode')->text(),
                                'charCode' => $i->filter('CharCode')->text(),
                                'name' => $i->filter('Name')->text(),
                                'value' => $i->filter('Value')->text(),
                                'date' => Carbon::create($dateTime)->timestamp
                            ]);
                        });

                        print 'Created ' . $dateTime . "\n";
                    }
                });
            }
        }

        print 'Finish';
    }
}
