<?php

namespace App\Services;

interface ScrapeInterface
{
    public function scrape();
}
