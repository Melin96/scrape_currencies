<?php

namespace App\Scopes;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Http\Request;

class CurrenciesScope implements Scope
{

    /**
     * Date from.
     *
     * @var Carbon
     */
    protected $from;

    /**
     * Date to.
     *
     * @var Carbon
     */
    protected $to;

    /**
     * Constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->from = $request->get('from') ? Carbon::create($request->get('from'))->timestamp : null;
        $this->to = $request->get('to') ? Carbon::create($request->get('to'))->timestamp : null;
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param Builder $builder
     * @param Model $model
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder
            ->when($this->from, function (Builder $query) {
                $query->where('date' , '>=', $this->from);
            })
            ->when($this->to, function (Builder $query) {
                $query->where('date' , '<=', $this->to);
            });
    }
}
