<?php

namespace App\Console\Commands;

use App\Accounts\CurrencyGateWay;
use App\Classes\Commands\ScrapeCurrencies;
use Illuminate\Console\Command;
use Goutte\Client;
use App\Currency;


class ScrapeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:currencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets currencies for 30 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app(ScrapeCurrencies::class)->run();
    }
}
