<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{

    protected $fillable = [
        'valuteID',
        'numCode',
        'charCode',
        'name',
        'value',
        'date'
    ];
}
